package com.mirhoseini.autolabs.weather;

import com.mirhoseini.autolabs.service.WeatherApi;
import com.mirhoseini.autolabs.util.Constants;
import com.mirhoseini.autolabs.util.SchedulerProvider;

import org.openweathermap.model.WeatherCurrent;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscription;
import rx.subjects.ReplaySubject;

/**
 * Created by Mohsen on 03/01/2017.
 */

@Weather
public class WeatherInteractorImpl implements WeatherInteractor {
    private WeatherApi api;
    private SchedulerProvider scheduler;

    private ReplaySubject<WeatherCurrent> weatherSubject;
    private Subscription weatherSubscription;

    @Inject
    public WeatherInteractorImpl(WeatherApi api, SchedulerProvider scheduler) {
        this.api = api;
        this.scheduler = scheduler;
    }


    @Override
    public Observable<WeatherCurrent> loadWeather(String city) {
        if (weatherSubscription == null || weatherSubscription.isUnsubscribed()) {
            weatherSubject = ReplaySubject.create();

            weatherSubscription = api.getMyWeather("http://fetapi.ir/weather.php",city, Constants.API_KEY, Constants.WEATHER_UNITS)
                    .subscribeOn(scheduler.backgroundThread())
                    .subscribe(weatherSubject);
        }

        return weatherSubject;
    }

    @Override
    public void destroy() {
        if (null != weatherSubscription && !weatherSubscription.isUnsubscribed())
            weatherSubscription.unsubscribe();
    }
}
