package com.mirhoseini.autolabs.weather;

import com.mirhoseini.autolabs.util.SchedulerProvider;
import com.mirhoseini.autolabs.util.StateManager;

import java.util.NoSuchElementException;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscription;
import rx.subscriptions.Subscriptions;

/**
 * Created by Mohsen on 03/01/2017.
 */

@Weather
public class WeatherPresenterImpl implements WeatherPresenter {

    WeatherInteractor interactor;
    StateManager stateManager;
    private WeatherView view;
    private Subscription subscription = Subscriptions.empty();
    private SchedulerProvider scheduler;

    @Inject
    public WeatherPresenterImpl(StateManager stateManager, SchedulerProvider scheduler, WeatherInteractor interactor) {
        this.interactor = interactor;
        this.scheduler = scheduler;
        this.stateManager = stateManager;
    }

    @Override
    public void bind(WeatherView view) {
        this.view = view;
    }

    @Override
    public void unbind() {
        this.view = null;

        if (null != subscription && !subscription.isUnsubscribed())
            subscription.unsubscribe();
    }

    @Override
    public void loadWeather(String[] cities) {
        if (view != null) {
            view.showProgress();
            view.updateProgressMessage("Loading City Weather...");
        }

        cities=new String[]{"q","b","q","d","e"};

        subscription = Observable.from(cities)
                .concatMap(city -> interactor.loadWeather(city)
                        .onErrorResumeNext(Observable.empty()))
                .filter(weatherCurrent -> weatherCurrent != null)
                .first()
                .observeOn(scheduler.mainThread())
                .subscribe(
                        weatherCurrent -> {
                            if (view != null) {
                                view.setWeatherValues(weatherCurrent);
                            }
                        },
                        throwable -> {
                            if (stateManager.isConnect()) {
                                if (view != null) {
                                    if (throwable.getClass().equals(NoSuchElementException.class)) {
                                        view.showToastMessage("City not found!!!");
                                    } else {
//                                view.showToastMessage(throwable.getMessage());
                                        view.showRetryMessage(throwable);
                                    }
                                }
                            } else {
                                if (view != null) {
                                    view.showConnectionError();
                                }
                            }

                            if (view != null) {
                                view.hideProgress();
                            }
                        },
                        () -> {
                            if (view != null) {
                                view.hideProgress();
                            }
                        });
    }

}
